const gulp = require('gulp');
const ts = require('gulp-typescript');
const watch = require('gulp-watch');
const jasmine = require('gulp-jasmine');
const clean = require('gulp-clean');
const copy = require('gulp-copy');


gulp.task('compile-src', () => {
    let tsProject = ts.createProject('tsconfig.json');
    return gulp.src("src/**/*.ts")
        .pipe(tsProject())
        .pipe(gulp.dest('src'));
});

gulp.task('compile-spec', () => {
    let tsProject = ts.createProject('tsconfig.json');
    return gulp.src("spec/**/*.ts")
        .pipe(tsProject())
        .pipe(gulp.dest('spec'));
});

gulp.task('compile', ['compile-src', 'compile-spec']);



gulp.task('jasmine', () => {
    gulp.src('spec/**/*.js')
        .pipe(jasmine({
            verbose: true,
            includeStackTrace: true,
            errorOnFail: true
        }));
});

gulp.task('test', ['clean', 'compile'], () => {
    gulp.start('jasmine');
});



gulp.task('clean-src', () => {
    gulp.src([
        'src/**/*.js', 
        'src/converters/*.js',
        'src/expressions/*.js'], { read: false, force: true })
            .pipe(clean());
});

gulp.task('clean-spec', () => {
    gulp.src('spec/**/*.js', { read: false })
        .pipe(clean());
});

gulp.task('clean-built', () => {
    gulp.src('built/*', { read: false })
        .pipe(clean());
});

gulp.task('clean', ['clean-src', 'clean-spec', 'clean-built']);



gulp.task('watch', () => {
    gulp.watch('src/**/*.ts').on('change', () => {
        gulp.start('typescript');
    });
});

gulp.task('watch', ['watch']);



gulp.task('copy', () => {
    return gulp.src(['src/**/*.js'])
        .pipe(gulp.dest('./built'));
});

gulp.task('build', ['test'], () => {
    gulp.start('copy');
});

