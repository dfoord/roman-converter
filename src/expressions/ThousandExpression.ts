import { Expression } from "./Expression";

export class ThousandExpression extends Expression {

  public multiplyer: number = 1000;

  public romanOne: string = "M";
  public romanFour: string = " ";
  public romanFive: string = "|V";
  public romanNine: string = "|V|X";

}