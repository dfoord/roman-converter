import { Expression } from "./Expression";

export class HundredExpression extends Expression {

  public multiplyer: number = 100;

  public romanOne: string = "C";
  public romanFour: string = "CD";
  public romanFive: string = "D";
  public romanNine: string = "CM";

}