import {Expression} from "./Expression";

export class TenExpression extends Expression {

  public multiplyer:number = 10;

  public romanOne:string = "X";
  public romanFour:string = "XL";
  public romanFive:string = "L";
  public romanNine:string = "XC";

}