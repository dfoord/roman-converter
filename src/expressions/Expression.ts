export abstract class Expression {

    private one: number = 1;
    private four: number = 4;
    private five: number = 5;
    private nine: number = 9;
    
    decimalOne(): number {
        return this.one * this.multiplyer;
    }

    decimalFour(): number {
        return this.four * this.multiplyer;
    }

    decimalFive(): number {
        return this.five * this.multiplyer;
    }

    decimalNine(): number {
        return this.nine * this.multiplyer;
    }

    public abstract multiplyer: number;

    public abstract romanOne: string;
    public abstract romanFour: string;
    public abstract romanFive: string;
    public abstract romanNine: string;

}