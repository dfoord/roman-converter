import { Expression } from "./Expression";

export class OneExpression extends Expression {

  public multiplyer: number = 1;

  public romanOne: string = "I";
  public romanFour: string = "IV";
  public romanFive: string = "V";
  public romanNine: string = "IX";

}