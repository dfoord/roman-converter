import { Expression } from "../expressions/Expression";
import { OneExpression } from "../expressions/OneExpression";
import { TenExpression } from "../expressions/TenExpression";
import { HundredExpression } from "../expressions/HundredExpression";
import { ThousandExpression } from "../expressions/ThousandExpression";

export class RomanToDecimalConverter {

  convert(roman: string): number {

    var expressions: Array<Expression> = [
      new ThousandExpression(),
      new HundredExpression(),
      new TenExpression(),
      new OneExpression()
    ];

    let output: number = 0;
    let input: string = roman;

    expressions.forEach(expr => {
      let matched: boolean = true;
      do {
        if (input.startsWith(expr.romanNine)) {
          output += expr.decimalNine();
          input = input.slice(expr.romanNine.length);
        } else if (input.startsWith(expr.romanFive)) {
          output += expr.decimalFive();
          input = input.slice(expr.romanFive.length);
        } else if (input.startsWith(expr.romanFour)) {
          output += expr.decimalFour();
          input = input.slice(expr.romanFour.length);
        } else if (input.startsWith(expr.romanOne)) {
          output += expr.decimalOne();
          input = input.slice(expr.romanOne.length);
        } else {
          matched = false;
        }
      } while (matched);
    });

    return output;

  }

}