import { Expression } from "../expressions/Expression";
import { OneExpression } from "../expressions/OneExpression";
import { TenExpression } from "../expressions/TenExpression";
import { HundredExpression } from "../expressions/HundredExpression";
import { ThousandExpression } from "../expressions/ThousandExpression";

export class DecimalToRomanConverter {

  convert(decimal: number): string {

    var expressions: Array<Expression> = [
      new ThousandExpression(),
      new HundredExpression(),
      new TenExpression(),
      new OneExpression()
    ];

    let output: string = "";
    let input: number = decimal;

    expressions.forEach(expr => {
      let matched: boolean = true;
      do {
        if (input / expr.decimalNine() >= 1) {
          output += expr.romanNine;
          input -= expr.decimalNine();
        } else if (input / expr.decimalFive() >= 1) {
          output += expr.romanFive;
          input -= expr.decimalFive();
        } else if (input / expr.decimalFour() >= 1) {
          output += expr.romanFour;
          input -= expr.decimalFour();
        } else if (input / expr.decimalOne() >= 1) {
          output += expr.romanOne;
          input -= expr.decimalOne();
        } else {
          matched = false;
        }
      } while (matched);
    });

    return output;

  }

}