/// <reference path="../typings/index.d.ts" />

import { RomanToDecimalConverter } from "./converters/RomanToDecimalConverter";

class RomanConveter {
  static main() {
    try {

      let romanConveter = new RomanToDecimalConverter();

      let romanNumerals: Array<string> = [
        "DCCXXXVII", //737
        "MXCV", //1095
        "CMLXXXVI", //986
        "LXXIV", //74
        "CCCLXVII" //367
      ];

      romanNumerals.forEach(roman => {
        let decimalOutput = romanConveter.convert(roman);
        console.log(`${roman} -> ${decimalOutput}`);
      });

      process.exit(0);

    } catch (err) {
      console.log(err);
      process.exit(1);
    }
  }
}

RomanConveter.main();