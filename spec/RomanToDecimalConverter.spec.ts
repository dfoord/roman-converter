/// <reference path="../typings/index.d.ts" />

import { RomanToDecimalConverter } from "../src/converters/RomanToDecimalConverter";

var romanConveter;

let romanNumerals: Array<string> = [
  "DCCXXXVII", //737
  "MXCV", //1095
  "CMLXXXVI", //986
  "LXXIV", //74
  "CCCLXVII" //367
];

let expected: Array<number> = [
  737,
  1095,
  986,
  74,
  367
];

describe("RomanToDecimalConverter", function () {

  beforeEach(() => {
    romanConveter = new RomanToDecimalConverter();
  });

  afterEach(() => {
    romanConveter = null;
  });

  romanNumerals.forEach((roman, i) => {
    it(`convert: ${roman} -> ${expected[i]}`, () => {
      let decimalOutput = romanConveter.convert(romanNumerals[i]);
      expect(decimalOutput).toBe(expected[i]);
    });
  });

});
