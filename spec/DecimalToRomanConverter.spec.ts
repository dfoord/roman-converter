/// <reference path="../typings/index.d.ts" />

import { DecimalToRomanConverter } from "../src/converters/DecimalToRomanConverter";

var decimalConveter;

let decimals: Array<number> = [
  737,
  1095,
  986,
  74,
  367
];

let expected: Array<string> = [
  "DCCXXXVII", //737
  "MXCV", //1095
  "CMLXXXVI", //986
  "LXXIV", //74
  "CCCLXVII" //367
];

describe("DecimalToRomanConverter", () => {

  beforeEach(() => {
    decimalConveter = new DecimalToRomanConverter();
  });

  afterEach(() => {
    decimalConveter = null;
  });

  decimals.forEach((decimal, i) => {
    it(`convert: ${decimal} -> ${expected[i]}`, () => {
      let romanOutput = decimalConveter.convert(decimals[i]);
      expect(romanOutput).toBe(expected[i]);
    });
  });

});
